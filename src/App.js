import './styles/App.css';
import { useState } from 'react';
import FormTemplate from './components/formTemplate/FormTemplate';
import Header from './components/header/Header';
import ImageTemplate from './components/imageTemplate/ImageTemplate';

function App() {

  const [stage, setStage] = useState(1);

  const [values, setValues] = useState({
    firstName: "",
    lastName: "",
    dob: "",
    email: "",
    address: "",
    message: "",
    choice2: "formTwoRadio1",
    choice3: "formThreeRadioOne"
  });

  const [errors, setErrors] = useState({});

  const [boySelected, setBoySelected] = useState(true);

  const [girlSelected, setGirlSelected] = useState(false);

  const [isSubmitted, setIsSubmitted] = useState(false);

  function handleAction(i) {

    setStage(i);

  }

  function validation(name, value) {

    if(!value || value === ""){

      setErrors({...errors, [name]: "Required field"});

    }
    else{

      setErrors({...errors, [name]: ""});

    }

    if(name && value) {

      if(name === "firstName"){

        const pattern = /^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/;

        if(pattern.test(value)){

          return true;

        }
        else{

          setErrors({...errors, [name]: "Only letters are allowed"});

        }

      }

      else if(name === "lastName"){

        const pattern = /^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/;

        if(pattern.test(value)){

          return true;

        }
        else{

          setErrors({...errors, [name]: "Only letters are allowed"});

        }

      }

      else if(name === "dob"){

        return true;

      }

      else if(name === "email"){

        const pattern = 	/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        if(pattern.test(value)){

          return true;

        }
        else{

          setErrors({...errors, [name]: "Enter a vaild email ID"});

        }

      }

      else if(name === "address"){

        return true;

      }

      else if(name === "message"){

        return true;

      }

    }

    return false;

  }

  function setValueAction(name, value){

    if(validation(name,value)){
      
      setValues({...values, [name]: value});
    
    }
    else{
      setValues({...values, [name]: value});
    }

  }

  function setErrorAction(name,value) {

    setErrors({...errors, [name]: value});

  }

  return (
    <div className="container">
      <div className="img-template">
        <ImageTemplate
          stage={stage}
        />
      </div>
      <div className="form-template">
        <Header 
          stage={stage}
        />
        <hr />
        <FormTemplate 
          stage={stage}
          setStage={(i) => handleAction(i)}
          setValues={(name, value) => setValueAction(name, value)}
          values={values}
          errors={errors}
          setErrors={(name, value) => setErrorAction(name, value)}
          boySelected={boySelected}
          girlSelected={girlSelected}
          setGirlSelected={(isGirlSelected) => setGirlSelected(isGirlSelected)}
          setBoySelected={(isBoySelected) => setBoySelected(isBoySelected)}
          isSubmitted={isSubmitted}
          setIsSubmitted={(isSubmitted) => setIsSubmitted(isSubmitted)}
        />
      </div>
    </div>
  );
}

export default App;
