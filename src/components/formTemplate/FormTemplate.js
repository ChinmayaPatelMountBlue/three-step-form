export default function FormTemplate(props){

    function getImageClassOne() {

        if(props.boySelected){
            return "checkbox-img border-blue";
        }

        return "checkbox-img";

    }

    function getImageClassTwo() {

        if(props.girlSelected){
            return "checkbox-img border-blue";
        }
        return "checkbox-img";

    }

    function imgClickBoy(event) {

        event.preventDefault();        

        props.setBoySelected(true);
        props.setGirlSelected(false);
        
        props.setValues("imgCheck","boy");

    }

    function imgClickGirl(event) {

        event.preventDefault();

        props.setBoySelected(false);
        props.setGirlSelected(true);
        
        props.setValues("imgCheck", "girl");

    }

    function handleChange(event) {

        props.setValues(event.target.name, event.target.value);

    }

    function handleSubmitOne(event) {

        event.preventDefault();

        let validatedFlag = true;

        if(props.values.firstName === ""){

            props.setErrors("firstName","Required field");

            validatedFlag  = false;

        }
        else if(!/^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/.test(props.values.firstName)){

            props.setErrors("Only letters allowed");
            validatedFlag  = false;

        }
        else if(props.values.lastName === ""){

            props.setErrors("lastName","Required field");

            validatedFlag  = false;

        }
        else if(!/^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/.test(props.values.lastName)){

            props.setErrors("Only letters allowed");

            validatedFlag  = false;
        }
        else if(props.values.dob === ""){

            props.setErrors("dob","Required field");

            validatedFlag  = false;
        }
        else if(props.values.email === ""){

            props.setErrors("email","Required field");

            validatedFlag  = false;
        }
        else if(!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(props.values.email)){

            props.setErrors("email","Enter a vaild email ID");

            validatedFlag  = false;
        }
        else if(props.values.address === ""){

            props.setErrors("address","Required field");

            validatedFlag  = false;
        }

        if(validatedFlag){
            props.setStage(2);
        }

    }

    function handleSubmitTwo(event) {

        event.preventDefault();

        let validatedFlag = true;

        if(props.values.message === ""){

            props.setErrors("message","Required field");
            validatedFlag = false;

        }
        if(props.values.choice2 === ""){

            props.setErrors("choice2","Required field");
            validatedFlag = false;

        }

        if(validatedFlag){
            props.setStage(3);
        }

    }

    function handleSubmitThree(event) {

        event.preventDefault();

        if(props.values.choice3 === ""){

            props.setErrors("choice3","Required FIeld");

            return;

        }

        props.setIsSubmitted(true);

    }

    const formTemplateOne = <>

        <h2 className="header-text">Sign Up</h2>

        <form className="form-container-one" onSubmit={handleSubmitOne} >

            <div className="name-block">

                <div className="first-name">
                    <label htmlFor="firstName">First Name</label>
                    <input
                        type="text"
                        id="firstName"
                        name="firstName"
                        onChange={handleChange}
                        value={props.values.firstName}
                        className={props.errors.firstName ? "first-name-input err-input" : "first-name-input" }
                   />
                    <p className={props.errors.firstName ? "visible err-msg" : "invisible"}>{props.errors.firstName}</p>
                </div>

                <div className="last-name">
                    <label htmlFor="lastName">Last Name</label>
                    <input
                        type="text"
                        id="lastName"
                        name="lastName"
                        onChange={handleChange}
                        value={props.values.lastName}
                        className={props.errors.lastName ? "last-name-input err-input" : "last-name-input" }
                    />
                    <p className={props.errors.lastName ? "visible err-msg" : "invisible"}>{props.errors.lastName}</p>
                </div>

            </div>

            <div className="details-block">
                
                <div className="dob">
                    <label htmlFor="dob">Date of Birth</label>
                    <input
                        type="date"
                        id="dob"
                        name="dob"
                        onChange={handleChange}
                        value={props.values.dob}
                        className={props.errors.dob ? "dob-input err-input" : "dob-input" } 
                    />
                    <p className={props.errors.dob ? "visible err-msg" : "invisible"}>{props.errors.dob}</p>
                </div>

                <div className="emailAddress">
                    <label htmlFor="emailId">Email Address</label>
                    <input
                        type="email"
                        id="email"
                        name="email"
                        onChange={handleChange}
                        value={props.values.email}
                        className={props.errors.email ? "email-input err-input" : "email-input" }
                    />
                    <p className={props.errors.email ? "visible err-msg" : "invisible"}>{props.errors.email}</p>
                </div>

            </div>

            <div className="address-block">
                <label htmlFor="address">Address</label>
                <input
                    type="text"
                    id="address"
                    name="address"
                    onChange={handleChange}
                    value={props.values.address}
                    className={props.errors.address ? "address-input err-input" : "address-input" }
                />
                <p className={props.errors.address ? "visible err-msg" : "invisible"}>{props.errors.address}</p>
            </div>

            <hr />

            <div className="btn-block-one">
                <button
                    type="submit"
                    className="btn-next btn-blue"
                >
                    Next Step
                </button>
            </div>
        </form>
        

    </>;

    const formTemplateTwo = <>
        <h2 className="header-text">Message</h2>

        <form className="form-container-two" onSubmit={handleSubmitTwo} >

            <div className="message-block">
                <label htmlFor="message">Message</label>
                <textarea
                    className={props.errors.message ? "msg-input err-input" : "msg-input"}
                    id="message"
                    name="message"
                    rows="10"
                    onChange={handleChange}
                    value={props.values.message}
                >
                </textarea>
                <p className={props.errors.message ? "visible err-msg" : "invisible"}>{props.errors.message}</p>
            </div>

            <div className={!(props.values.choice2) ? "radio-block err-outline" : "radio-block" }>
                <div><input id="formTwoRadio1" name="choice2" type="radio" value="formTwoRadio1" onChange={handleChange} checked={((props.values.choice2 === "") || (props.values.choice2 === "formTwoRadio1") ? "checked" : "")} /><label htmlFor="formTwoRadio1">The number one choice</label></div>
                <div><input id="formTwoRadio2" name="choice2" type="radio" value="formTwoRadio2" onChange={handleChange} checked={((props.values.choice2 === "formTwoRadio2") ? "checked" : "")} /><label htmlFor="formTwoRadio2">The number two choice</label></div>
            </div>
            <p className={!(props.values.choice2) ? "err-msg visible" : "invisible" }>{props.errors.choice2}</p>

            <hr />

            <div className="btn-block-two">
                    <div className="btn-back" onClick={() => props.setStage(1)}>Back</div>
                    <button
                        className="btn-blue"
                        type="submit"
                    >
                        Next Step
                    </button>
            </div>

        </form>
    </>;

    const formTemplateThree = <>

        <h2 className="header-text">Checkbox</h2>

        <form className="form-container-three" onSubmit={handleSubmitThree} >

            <div className="img-container">

                <button
                    className="clickable-images"
                    onClick={imgClickBoy}
                >
                    <img
                        src={process.env.PUBLIC_URL + "images/boy.png"}
                        alt=""
                        className={getImageClassOne()}
                    />
                </button>
                <button
                    className="clickable-images"
                    onClick={imgClickGirl}
                >
                    <img src={process.env.PUBLIC_URL + "images/girl.png"}
                        alt=""
                        className={getImageClassTwo()}
                    />
                </button>

            </div>
            <p className={(props.boySelected || props.girlSelected) ? "invisible" : "err-msg" }>{props.errors.choice3}</p>

            <div className={!(props.values.choice3) ? "radio-container err-outline" : "radio-container"}>

                <div><input type="radio" name="choice3" value="formThreeRadioOne" id="formThreeRadioOne" onChange={handleChange} checked={((props.values.choice3 === "") || (props.values.choice3 === "formThreeRadioOne")) ? "checked" : "" } /><label htmlFor="formThreeRadioOne"> I want to add this option.</label></div>
                <div><input type="radio" name="choice3" value="formThreeRadioTwo" id="formThreeRadioTwo" onChange={handleChange} checked={(props.values.choice3 === "formThreeRadioTwo") ? "checked" : "" } /><label htmlFor="formThreeRadioTwo"> Let me click on this checkbox and choose some cool stuff.</label></div>

            </div>
            <p className={!(props.values.choice3) ? "err-msg visible" : "invisible" }>{props.errors.choice3}</p>

            <hr />

            <div className="btn-block-three">

                <div className="btn-back" onClick={(isSubmitted) => {props.setStage(2); props.setIsSubmitted(false)}} >Back</div>
                <button
                    className="btn-blue btn-submit"
                    type="submit"
                >
                    Submit
                </button>

            </div>

        </form>

        <div className={props.isSubmitted ? "form-submit-text" : "invisible" }>

            <div >Your details have been submitted!</div>

        </div>

    </>;

    let formTemplate;
    if(props.stage === 1){
        formTemplate = formTemplateOne;
    }
    else if(props.stage === 2){
        formTemplate = formTemplateTwo;
    }
    else if(props.stage === 3){
        formTemplate = formTemplateThree;
    }

    return (
        <>
            <div className="step-text">Step{props.stage}/3</div>
            {formTemplate}
        </>
    );

}