export default function Header(props){

    function getClassBoxOne(){
        if(props.stage === 1){
            return "text-in-box blue-container";
        }
        else{
            return "text-in-box";
        }
    }

    function getClassBoxTwo(){
        if(props.stage === 2){
            return "text-in-box blue-container";
        }
        else{
            return "text-in-box";
        }
    }

    function getClassBoxThree(){
        if(props.stage === 3){
            return "text-in-box blue-container";
        }
        else{
            return "text-in-box";
        }
    }


    return (
        <div className="header-container">

            <div className="header-box">
                <div className={getClassBoxOne()}>{(props.stage > 1) ? <img src={process.env.PUBLIC_URL + "images/blue-check-mark.png"} className="tick-mark" alt=""/> : 1 }</div>
                <div className="header-text">Sign Up</div>
            </div>

            <div className="header-box">
                <div className={getClassBoxTwo()} id="twoBox">{(props.stage > 2) ? <img src={process.env.PUBLIC_URL + "images/blue-check-mark.png"} className="tick-mark" alt="" /> : 2 }</div>
                <div className="header-text">Message</div>
            </div>

            <div className="header-box">
                <div className={getClassBoxThree()} id="threeBox">3</div>
                <div className="header-text">Checkbox</div>
            </div>

        </div>
    );

}