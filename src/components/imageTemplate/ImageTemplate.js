export default function ImageTemplate(props){

    const imgOne = process.env.PUBLIC_URL + "images/1.png";
    const imgTwo = process.env.PUBLIC_URL + "images/2.png";
    const imgThree = process.env.PUBLIC_URL + "images/3.png";

    let img;

    if(props.stage === 1){
        img = imgOne;
    }
    else if(props.stage === 2){
        img = imgTwo;
    }
    else if(props.stage === 3){
        img = imgThree;
    }

    return (
        <>
            <img src={img} alt="" className="img-aside" />
        </>
    );

}